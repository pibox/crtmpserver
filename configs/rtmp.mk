# ---------------------------------------------------------------
# Build crtmpserver
# ---------------------------------------------------------------

opkg-verify:
	@if [ "$(OPKG_DIR)" = "" ] || [ ! -f $(OPKG_DIR)/opkg-build ]; then \
		$(MSG11) "Can't find opkg-build.  Try setting OPKG= to the directory it lives in." $(EMSG); \
		exit 1; \
	fi

$(RTMP_T)-verify:
	@if [ "$(CROSS_COMPILER)" = "" ] || [ ! -f $(CROSS_COMPILER) ]; then \
		$(MSG11) "Can't find cross toolchain.  Try setting XI= on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ "$(SD)" = "" ] || [ ! -d $(SD) ]; then \
		$(MSG11) "Can't find staging tree.  Try setting SD= on the command line." $(EMSG); \
		exit 1; \
	fi

# Retrieve package
.$(RTMP_T)-get $(RTMP_T)-get: 
	@mkdir -p $(BLDDIR) 
	@if [ ! -d $(RTMP_SRCDIR) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) "Retrieving RTMP source" $(EMSG); \
		$(MSG) "================================================================"; \
		cd $(BLDDIR) && svn co --username anonymous --password "" $(RTMP_URL) $(RTMP_VERSION); \
	else \
		$(MSG3) "RTMP source is cached" $(EMSG); \
	fi
	@touch .$(subst .,,$@)

.$(RTMP_T)-get-patch $(RTMP_T)-get-patch: .$(RTMP_T)-get
	@touch .$(subst .,,$@)

# Unpack packages
.$(RTMP_T)-unpack $(RTMP_T)-unpack: .$(RTMP_T)-get-patch
	@touch .$(subst .,,$@)

# Apply patches
.$(RTMP_T)-patch $(RTMP_T)-patch: .$(RTMP_T)-unpack
	@$(MSG) "================================================================"
	@$(MSG3) "Patching RTMP source" $(EMSG)
	@$(MSG) "================================================================"
	@cd $(RTMP_SRCDIR) && patch -p1 < $(DIR_RTMP)/patches/compile.patch
	@cp $(DIR_RTMP)/patches/pibox.mk $(RTMP_SRCDIR)/builders/make
	@touch .$(subst .,,$@)

.$(RTMP_T)-init $(RTMP_T)-init: 
	@make $(RTMP_T)-verify
	@make .$(RTMP_T)-patch
	@touch .$(subst .,,$@)

# Build the package
$(RTMP_T): .$(RTMP_T)

.$(RTMP_T): .$(RTMP_T)-init 
	@$(MSG) "================================================================"
	@$(MSG2) "Building RTMP" $(EMSG)
	@$(MSG) "================================================================"
	@cd $(RTMP_SRCDIR)/builders/make && make SD=$(SD) TC=$(XCC_PREFIXDIR) PLATFORM=pibox
	@touch .$(subst .,,$@)

$(RTMP_T)-files:
	@$(MSG) "================================================================"
	@$(MSG2) "RTMP Build Files ($(RTMP_SRCDIR)/builders/make/output)" $(EMSG)
	@$(MSG) "================================================================"
	@ls -l $(RTMP_SRCDIR)/builders/make/output

# Package it as an opkg 
opkg pkg $(RTMP_T)-pkg: .$(RTMP_T) opkg-verify
	@mkdir -p $(RTMP_BLDDIR)/opkg/crtmpserver/{CONTROL,etc/ld.so.conf.d,usr/lib/crtmpserver,usr/bin,etc/init.d}
	@cp -ar $(RTMP_SRCDIR)/builders/make/output/dynamic/crtmpserver $(RTMP_BLDDIR)/opkg/crtmpserver/usr/bin
	@cp -ar $(RTMP_SRCDIR)/builders/make/output/dynamic/*.so $(RTMP_BLDDIR)/opkg/crtmpserver/usr/lib/crtmpserver
	@cp -ar $(RTMP_SRCDIR)/builders/make/output/dynamic/applications $(RTMP_BLDDIR)/opkg/crtmpserver/usr/lib/crtmpserver
	@cp -ar $(RTMP_SRCDIR)/builders/make/output/dynamic/crtmpserver.lua $(RTMP_BLDDIR)/opkg/crtmpserver/etc
	@cp $(SRCDIR)/opkg/crtmpserver.conf $(RTMP_BLDDIR)/opkg/crtmpserver/etc/ld.so.conf.d
	@cp $(SRCDIR)/opkg/crtmpserver.lua $(RTMP_BLDDIR)/opkg/crtmpserver/etc/crtmpserver.lua
	@cp $(SRCDIR)/scripts/S90crtmpserver $(RTMP_BLDDIR)/opkg/crtmpserver/etc/init.d
	@cp $(SRCDIR)/opkg/control $(RTMP_BLDDIR)/opkg/crtmpserver/CONTROL/control
	@cp $(SRCDIR)/opkg/postinst $(RTMP_BLDDIR)/opkg/crtmpserver/CONTROL/postinst
	@cp $(SRCDIR)/opkg/prerm $(RTMP_BLDDIR)/opkg/crtmpserver/CONTROL/prerm
	@cp $(SRCDIR)/opkg/debian-binary $(RTMP_BLDDIR)/opkg/crtmpserver/CONTROL/debian-binary
	@chmod +x $(RTMP_BLDDIR)/opkg/crtmpserver/CONTROL/postinst
	@chmod +x $(RTMP_BLDDIR)/opkg/crtmpserver/CONTROL/prerm
	@cd $(RTMP_BLDDIR)/opkg/ && $(OPKG_DIR)/opkg-build -O crtmpserver
	@mkdir -p $(PKGDIR)/
	@cp $(RTMP_BLDDIR)/opkg/*.opk $(PKGDIR)/

# Clean the packaging
pkg-clean $(RTMP_T)-pkg-clean:
	@rm -rf $(PKGDIR) $(RTMP_BLDDIR)/*

# Clean out a cross compiler build but not the CT-NG package build.
$(RTMP_T)-clean:
	@cd $(RTMP_SRCDIR) && make PLATFORM=pibox clean
	@if [ "$(PKGDIR)" != "" ] && [ -d "$(PKGDIR)" ]; then rm *; fi
	@rm -f .$(RTMP_T) 

# Clean out everything associated with RTMP
$(RTMP_T)-clobber: 
	@rm -rf $(PKGDIR) $(RTMP_SRCDIR) $(RTMP_ARCDIR)
	@rm -f .$(RTMP_T)-init .$(RTMP_T)-patch .$(RTMP_T)-unpack .$(RTMP_T)-get .$(RTMP_T)-get-patch
	@rm -f .$(RTMP_T) 

